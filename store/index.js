// import axios from 'axios'
import floreria from "@/assets/images/floreria.jpg";
import panaderia from "@/assets/images/panaderia.jpg";
import interiorismo from "@/assets/images/interiorismo.jpg";
import lavanderia from "@/assets/images/lavanderia.jpg";
import accesorios from "@/assets/images/accesorios.jpg";
import pasteleria from "@/assets/images/pasteleria-2.jpg";
import restaurant from "@/assets/images/restaurant.jpg";
import tacos from "@/assets/images/tacos.jpg";
import verduleria from "@/assets/images/verduleria.jpg";

import dentista from "@/assets/images/dentista.jpg";
import barber from "@/assets/images/barber.jpg";

import autolavado from "@/assets/images/autolavado.jpg";
import veterinaria from "@/assets/images/veterinaria.jpg";
import mecanico from "@/assets/images/mecanico.jpg";
import carpinteria from "@/assets/images/carpinteria.jpg";

import karate from "@/assets/images/karate.jpg";
import gym from "@/assets/images/gym.jpg";

import sastre from "@/assets/images/sastre.jpg";

export const state = () => ({
	/*results: [{
					image: floreria,
					titulo: 'Interflora',
					genero: 'FPS',
					puntuacion: 9,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: panaderia,
					titulo: 'El buen sazón',
					genero: 'Estrategia',
					puntuacion: 10,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: interiorismo,
					titulo: 'SADIIQ',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: lavanderia,
					titulo: 'Laundry',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: carpinteria,
					titulo: 'Carpintería',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: accesorios,
					titulo: 'Accesorios',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: mecanico,
					titulo: 'Taller mecánico',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: pasteleria,
					titulo: 'Pastelería',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: dentista,
					titulo: 'Dentista',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: restaurant,
					titulo: 'Restaurant',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: sastre,
					titulo: 'Sastre',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: veterinaria,
					titulo: 'Maskotas',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: barber,
					titulo: 'Barbería',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: autolavado,
					titulo: 'autolavado',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: tacos,
					titulo: 'Taquería',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: gym,
					titulo: 'Gimnasio',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: verduleria,
					titulo: 'Verdulería',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},
				{
					image: karate,
					titulo: 'Karate',
					genero: 'Survival Horror',
					puntuacion: 7,
					catID: 1,
					catSlug: 'florerias',
					catName: 'florerías',
					catColor: 'bg-green-300'
				},],*/
	results: [],
	resultsFull : false,
	filterResults: [],
	searchString: '',
	typeSearch: 'all',
	item: {
		bName: 'Cargando...'
	}
})

export const mutations = {
	setResults(state, results) {
		state.results = results
	},
	setSearchString(state, string){
		state.searchString = string
	},
	setItem(state, item){
		state.item = item
	},
	setFilterResults(state, filterResults){
		state.filterResults = filterResults
	},
	setResultsFull(state, value){
		state.resultsFull = value
	},
	setTypeSearch(state, type){
		state.typeSearch = type
	}
}

export const actions = {
	setSearchStringAction(context, string){		
		context.commit('setSearchString',string);
	},

	setItemAction(context, item){
		context.commit('setItem',item)
	},

	setFilterResultsAction(context, array){
		context.commit('setFilterResults', array)
		context.commit('setResultsFull', true)
	},

	setTypeSearchAction(context, type){
		context.commit('setTypeSearch', type)
	},
}