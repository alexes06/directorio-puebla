export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'directoriopuebla',
    htmlAttrs: {
      lang: 'es'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    ['nuxt-fontawesome', {
      component: 'fa', //customize component name
      imports: [{set: '@fortawesome/free-solid-svg-icons', icons: ['faLightbulb','faStar'] },
          {set: '@fortawesome/free-brands-svg-icons', icons: ['faGithub']},
          {set: '@fortawesome/free-regular-svg-icons', icons: ['faLightbulb','faStar']},
      ]
   }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  env:{
    baseUrl: 'https://backend.directoriopuebla.com',
    devBaseUrl: 'http://localhost:8000'
  },

  server: {
    host:'0'
  },

}
